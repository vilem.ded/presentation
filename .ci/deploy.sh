#!/bin/bash

PAGEURL="https://r3.pages.uni.lu/outreach/templates/presentation"

rootDir=$(pwd)
echo $rootDir

# clean previous builds
rm -rf $rootDir/public
mkdir $rootDir/public

# loop through all the presentations
nBlades=0
for d in $(find . -maxdepth 1 -type d)
do
    if [[ "${d}" == *20* ]]; then
        cd "${d}"
        echo "changed to ${d}"
        for d1 in $(find . -maxdepth 1 -type d)
        do
            if [[ ${#d1} -ge 2 ]]; then
                let "nBlades+=1"
                #blades[$nBlades]="$tutorial"
                BLADE=${d1:2}
                echo " > -----------------------------------------------------"
                echo "Building ... ${d1}"

                # create the required folders in public
                mkdir $rootDir/public/$BLADE
                mkdir $rootDir/public/$BLADE/unstable
                mkdir $rootDir/public/$BLADE/latest
                mkdir $rootDir/public/$BLADE/stable

                # change to the blade directory to be built
                cd $BLADE

                # build the unstable version - can be optimized
                git config submodule.theme.url https://$CI_DEPLOY_USER:$CI_DEPLOY_PASSWORD@git-r3lab.uni.lu/R3-core/outreach/theme.git
                git submodule update --recursive --init

                # run contribute
                cp ../../template/Gruntfile.coffee .
                ln -s ../../theme/package.json package.json
                ln -s ../../theme theme

                # install npm
                #npm install -g npm@latest yo grunt-cli generator-reveal
                #npm install
                yarn global add grunt-cli generator-reveal
                yarn install

                grunt dist
                mv public/* $rootDir/public/$BLADE/unstable/.
                echo " > unstable built for file set ${BLADE}"

                # build the master version into stable
                git fetch --tags
                latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
                git stash
                git checkout $latestTag
                git submodule update --init
                grunt dist
                mv public/* $rootDir/public/$BLADE/stable/.
                echo " > stable built for file set ${BLADE}"

                # build the develop version into latest
                git stash
                git checkout -f develop
                git fetch origin develop
                git reset --hard origin/develop
                git submodule update --init
                grunt dist
                mv public/* $rootDir/public/$BLADE/latest/.
                echo " > latest built for file set ${BLADE}"

                # auto redirect file to the stable version
                echo '<html>\n
                    <head>\n
                    <meta http-equiv="refresh" content="0; url='$PAGEURL'/'$BLADE'/stable"/>\n
                    </head>\n
                    <body>\n
                    </bod>\n
                    </html>\n' > $rootDir/public/$BLADE/index.html

                echo "autoredirect file set"

                # move back out
                cd ..
                echo " > -----------------------------------------------------"
            fi
        done
    fi
done
