from setuptools import setup

setup(
    name='contribute',
    version='0.1',
    py_modules=['contribute'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        contribute=contribute:main
    ''',
)
